package fr.dawan.bibliotheque;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.services.LivreService;
@Component
public class ServiceRunner implements ApplicationRunner {

    @Autowired
    private LivreService service;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        service.getAllLivre(Pageable.unpaged()).forEach(System.out::println);
        System.out.println("-------------------------");
        System.out.println(service.getById(2));
        System.out.println("-------------------------");
        service.searchByTitre("%Dune%").forEach(System.out::println);
        
        
    }

}
