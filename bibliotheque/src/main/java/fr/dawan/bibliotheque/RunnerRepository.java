package fr.dawan.bibliotheque;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import fr.dawan.bibliotheque.repositories.AuteurRepository;
import fr.dawan.bibliotheque.repositories.LivreRepository;
//@Component
public class RunnerRepository implements ApplicationRunner {

    @Autowired
    AuteurRepository auteurRepo;
    
    @Autowired
    LivreRepository livreRepo;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("\nAuteur vivant");
        auteurRepo.findByDecesIsNull().forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nAuteur vivant pagination");
        auteurRepo.findByDecesIsNull(PageRequest.of(0, 2)).forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nAuteur par nom : Buzzati");
        auteurRepo.findByNom("Buzzati").forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nAuteur par Id livre");
        auteurRepo.findByLivreId(133).forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nClassement des Auteurs par nombre de livre écrit");
        auteurRepo.findByTopAuteur().forEach(System.out::println);
        System.out.println("\n\nLivre dont le titre commence par D et fait au minimm 3 caractères");
        livreRepo.findByTitreLike("D__%").forEach(System.out::println);;
        System.out.println("-----------------------------------------");
        System.out.println("\nLivre sortie en 1992");
        livreRepo.findByAnneeSortie(1992).forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nLivre sortie entre 1980 et 1990");
        livreRepo.findByAnneeSortieBetweenOrderByAnneeSortie(1980, 1990).forEach(System.out::println);
        System.out.println("-----------------------------------------");
        System.out.println("\nLivre pour une categorie");
        livreRepo.findByNomCategorie("Science-fiction").forEach(System.out::println);
    }

}
