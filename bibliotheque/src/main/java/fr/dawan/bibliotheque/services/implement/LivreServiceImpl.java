package fr.dawan.bibliotheque.services.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import fr.dawan.bibliotheque.dto.LivreDto;
import fr.dawan.bibliotheque.entities.Livre;
import fr.dawan.bibliotheque.repositories.LivreRepository;
import fr.dawan.bibliotheque.services.LivreService;

@Service
public class LivreServiceImpl implements LivreService {
    
    @Autowired
    private LivreRepository repository;
    
    @Autowired
    private ModelMapper mapper;
    
    @Override
    public List<LivreDto> getAllLivre(Pageable page) {
        return repository.findAll(page).getContent().stream().map(m -> mapper.map(m, LivreDto.class)).collect(Collectors.toList());    
    }

    @Override
    public LivreDto getById(long id) {
        return mapper.map(repository.findById(id).get(), LivreDto.class);
    }

    @Override
    public List<LivreDto> searchByTitre(String titre) {
        return repository.findByTitreLike(titre).stream().map(m -> mapper.map(m, LivreDto.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(long id) {
        repository.deleteById(id);

    }

    @Override
    public LivreDto saveOrUpdate(LivreDto lvrDto) {
        return mapper.map(repository.saveAndFlush(mapper.map(lvrDto, Livre.class)), LivreDto.class);
     }

}
