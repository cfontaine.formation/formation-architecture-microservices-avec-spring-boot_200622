package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.bibliotheque.entities.Livre;

public interface LivreRepository extends JpaRepository<Livre, Long> {
    public List<Livre> findByTitreLike(String modele);
    
    public List<Livre> findByAnneeSortie(int annee);
    
    public List<Livre> findByAnneeSortieBetweenOrderByAnneeSortie(int anneeMin,int anneeMax);
   
    @Query(value="FROM Livre l JOIN l.categorie c WHERE c.nom=:nomCategorie")
    public List<Livre> findByNomCategorie(@Param("nomCategorie")String nomCategorie);
}
