package fr.dawan.bibliotheque.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.bibliotheque.entities.Auteur;

public interface AuteurRepository extends JpaRepository<Auteur, Long> {

    List<Auteur> findByDecesIsNull();

    Page<Auteur> findByDecesIsNull(Pageable pageable);

    List<Auteur> findByNom(String nom);

    @Query(value = "FROM Auteur a Join a.livres l WHERE l.id=:idLivre")
    List<Auteur> findByLivreId(@Param("idLivre") long idLivre);
    
    @Query(value="FROM Auteur a JOIN a.livres l GROUP BY a.id ORDER BY COUNT(l.id) DESC")
    List<Auteur> findByTopAuteur();
}
