package fr.dawan.springboot.repositories;

import java.util.List;

import fr.dawan.springboot.entities.Employe;

public interface EmployeCustomRepository {

    List<Employe> findBy(String prenom, String nom);
}
