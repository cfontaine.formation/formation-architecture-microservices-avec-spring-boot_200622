package fr.dawan.springboot.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.springboot.entities.Marque;

public interface MarqueRepository extends JpaRepository<Marque, Long> {
    void deleteByNom(String nomMarque);
}
