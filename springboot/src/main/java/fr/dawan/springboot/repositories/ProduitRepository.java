package fr.dawan.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnment;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {

   List<Produit> findByPrixGreaterThan(double prix);
   
   List<Produit> findByPrixBetween(double min, double max);
   
   List<Produit> findByPrixGreaterThanAndEmbalage(double prix,Conditionnment emb);
   
   List<Produit> findByDescriptionLikeIgnoreCase(String model);
   
   List<Produit> findByPrixLessThanOrderByPrixDesc(double prix);
   
   List<Produit> findByEmbalage(Conditionnment embalage);
   
   // JPQL
   @Query(value="SELECT p FROM Produit p WHERE p.prix>:montant") // ?1
   List<Produit> findByPrixGrJPQL(@Param("montant")double prixMin);
   
   // SQL
   @Query(nativeQuery = true,value="SELECT * FROM produits WHERE prix>:montant")
   List<Produit> findByPrixGrSQL(@Param("montant")double prixMin);
   
   // Pagination
   
   Page<Produit> findByPrixLessThan(double prixMax,Pageable pageable);
   
   List<Produit> findByPrixGreaterThan(double prixMin,Pageable pageable);
   
   List<Produit> findByPrixGreaterThan(double prixMin,Sort sort);
   
   // LIMIt ou TOP
   List<Produit> findTop5ByOrderByPrix();
   
   // autre sujet
   
   boolean existsByPrix(double prix);
   
   int countByEmbalage(Conditionnment emb);

   // Procedure Stockée
   @Query(nativeQuery = true, value="CALL GET_COUNT_BY_PRIX(:montant)")
   int countByPrixSQL(@Param("montant") double montant);
   
   // Appel Explicite
   @Procedure("GET_COUNT_BY_PRIX2")
   int countByPrixExp(double montant);
   
   
   // Appel Implicite
   @Procedure
   int get_count_by_prix2(@Param("montant") double montant);
   
}
