package fr.dawan.springboot.services.implement;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnment;
import fr.dawan.springboot.mappers.ProduitMapper;
import fr.dawan.springboot.repositories.ProduitRepository;
import fr.dawan.springboot.services.ProduitService;

@Service
public class ProduitServiceImpl implements ProduitService {

    @Autowired
    private ProduitRepository prodRepository;

//    @Autowired
//    private ModelMapper modelMapper;
//    
    @Autowired
    private ProduitMapper mapper;

    @Override
    public List<ProduitDto> getAllProduit(Pageable page) {
//        List<Produit> lst=prodRepository.findAll(page).getContent(); 
//        List<ProduitDto> lstDto=new ArrayList<>();
//        for(Produit p :lst) {
//            lstDto.add(modelMapper.map(p, ProduitDto.class));
//        }
//        return lstDto;
       // return prodRepository.findAll(page).getContent().stream().map(m -> modelMapper.map(m, ProduitDto.class))
       //         .collect(Collectors.toList());
        
        return prodRepository.findAll(page).getContent().stream().map(mapper::toDto)
                         .collect(Collectors.toList());
    }

    @Override
    public ProduitDto getProduitById(long id) {
       // return modelMapper.map(prodRepository.findById(id).get(), ProduitDto.class);
        return mapper.toDto(prodRepository.findById(id).get());
    }

    @Override
    public List<ProduitDto> getProduitByEmbalage(Conditionnment embalage) {
        
//        return prodRepository.findByEmbalage(embalage).stream().map(e -> modelMapper.map(e,ProduitDto.class))
//                .collect(Collectors.toList());
        
        return prodRepository.findByEmbalage(embalage).stream().map(mapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProduitDto saveOrUpdate(ProduitDto produitDto) {
        
        
//        Produit tmp = prodRepository.saveAndFlush(modelMapper.map(produitDto, Produit.class));
//        return modelMapper.map(tmp, ProduitDto.class);
    
       Produit pr= prodRepository.saveAndFlush(mapper.fromDto(produitDto));
       return mapper.toDto(pr);
    }

    @Override
    public void deleteProduit(long id) {
        prodRepository.deleteById(id);
    }

}
