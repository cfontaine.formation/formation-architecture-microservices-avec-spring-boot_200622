package fr.dawan.springboot.services.implement;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import fr.dawan.springboot.entities.User;
import fr.dawan.springboot.repositories.UserRepository;

@Service
public class UserService implements UserDetailsService {
    
    private UserRepository repository;

    // @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userDb=null;
        if(username!=null) {
            userDb=repository.findByUsername(username);
            System.out.println(userDb);
        }
        if(userDb==null) {
            throw new UsernameNotFoundException("Utilisateur nom trouvé");
        }
        return userDb;
    }

}
