package fr.dawan.springboot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.dawan.springboot.services.implement.UserService;

public class AppAuthProvider extends DaoAuthenticationProvider {
    @Autowired
    UserService userService;
    
    @Autowired 
    PasswordEncoder encoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
        String name = auth.getName();
        String password = auth.getCredentials().toString();
        UserDetails user = userService.loadUserByUsername(name);
        System.out.println();
        if (password!=null && encoder.matches(password, user.getPassword())) {
            return new UsernamePasswordAuthenticationToken(user, null, null);
        }
        else {
            throw new BadCredentialsException("Mauvais mot de passe");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
