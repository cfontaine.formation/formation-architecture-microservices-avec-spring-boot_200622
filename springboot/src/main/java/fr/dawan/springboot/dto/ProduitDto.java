package fr.dawan.springboot.dto;

import java.time.LocalDate;
import java.util.Arrays;

import fr.dawan.springboot.enums.Conditionnment;

public class ProduitDto {
    
    private long id;
    
    private double prix;
    
    private String Description;
    
    private LocalDate dateConception;
    
    private Conditionnment embalage;
    
    private byte[] image;
    
    private MarqueDto marque;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public LocalDate getDateConception() {
        return dateConception;
    }

    public void setDateConception(LocalDate dateConception) {
        this.dateConception = dateConception;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public MarqueDto getMarque() {
        return marque;
    }

    public void setMarque(MarqueDto marque) {
        this.marque = marque;
    }

    public Conditionnment getEmbalage() {
        return embalage;
    }

    public void setEmbalage(Conditionnment embalage) {
        this.embalage = embalage;
    }

    @Override
    public String toString() {
        return "ProduitDto [id=" + id + ", prix=" + prix + ", Description=" + Description + ", dateConception="
                + dateConception + ", marque=" + marque + "]";
    }

}
