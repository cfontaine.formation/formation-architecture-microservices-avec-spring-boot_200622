package fr.dawan.springboot;

import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dto.MarqueDto;
import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.enums.Conditionnment;
import fr.dawan.springboot.services.ProduitService;

//@Component
public class ServiceRunner implements ApplicationRunner {

    @Autowired 
    ProduitService service;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
      service.getAllProduit(Pageable.unpaged()).forEach(System.out::println);
      
      System.out.println(service.getProduitById(3));
      
      service.getProduitByEmbalage(Conditionnment.CARTON).forEach(System.out::println);
  
      
      MarqueDto ma=new MarqueDto();
      ma.setId(2);
      ma.setNom("Marque B");
      
      ProduitDto p1=new ProduitDto();
      p1.setDateConception(LocalDate.of(2017, 10, 23));
      p1.setDescription("Convertissuer Hdmi DVI");
      p1.setPrix(15.0);
      p1.setMarque(ma);
      p1.setEmbalage(Conditionnment.SANS);
      ProduitDto pr=service.saveOrUpdate(p1);
      System.out.println(pr);
      
  //    service.deleteProduit(pr.getId());
    }

}
