package fr.dawan.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import fr.dawan.springboot.enums.Contrat;

@Entity
@Table(name = "employes")
// @TableGenerator(name = "myTableGen")
public class Employe extends AbstractEntity{//implements Serializable

    private static final long serialVersionUID = 1L;

//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    //@GeneratedValue(strategy= GenerationType.TABLE,generator = "myTableGen")
//    private long id;

    @Column(length = 60, nullable = false)
    private String prenom;

    @Column(length = 50, nullable = false)
    private String nom;

    @Enumerated(EnumType.STRING)
    @Column(length = 15,nullable= false)
    private Contrat contrat = Contrat.CDI;
    
    @Embedded
    private Adresse adressePerso;
    
    @Embedded
    @AttributeOverrides({
        @AttributeOverride(name="rue",column=@Column(name="rue_pro")),
        @AttributeOverride(name="ville",column=@Column(name="ville_pro")),
        @AttributeOverride(name="codePostal",column=@Column(name="code_postal_pro"))
    })
    private Adresse adressePro;
    @ElementCollection(targetClass = String.class)
    @CollectionTable(name="employe_telephone")
    private List<String> telephones=new ArrayList<>();

    public Employe() {

    }

    public Employe(String prenom, String nom) {

        this.prenom = prenom;
        this.nom = nom;
    }

//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Contrat getContrat() {
        return contrat;
    }

    public void setContrat(Contrat contrat) {
        this.contrat = contrat;
    }

    
    
    public Adresse getAdressePerso() {
        return adressePerso;
    }

    public void setAdressePerso(Adresse adressePerso) {
        this.adressePerso = adressePerso;
    }

    public Adresse getAdressePro() {
        return adressePro;
    }

    public void setAdressePro(Adresse adressePro) {
        this.adressePro = adressePro;
    }

    public List<String> getTelephones() {
        return telephones;
    }

    public void setTelephones(List<String> telephones) {
        this.telephones = telephones;
    }

    @Override
    public String toString() {
        return "Employe [prenom=" + prenom + ", nom=" + nom + ", contrat=" + contrat + ", adressePerso=" + adressePerso
                + ", adressePro=" + adressePro + ", " + super.toString() + "]";
    }

}
