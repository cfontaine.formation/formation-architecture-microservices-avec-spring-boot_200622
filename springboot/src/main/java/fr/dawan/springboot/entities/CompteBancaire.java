package fr.dawan.springboot.entities;

import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
@Entity
@Inheritance(strategy = InheritanceType.JOINED) // InheritanceType.SINGLE_TABLE
//@DiscriminatorColumn(name = "type_compte", discriminatorType = DiscriminatorType.STRING,length = 2)
//@DiscriminatorValue("BA")
public class CompteBancaire implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy =GenerationType.AUTO) // GenerationType.IDENTITY
    private long id;

    private double solde;
    
    private String iban;
    
    private String titulaire;

    public CompteBancaire() {

    }

    public CompteBancaire(double solde, String iban, String titulaire) {
        this.solde = solde;
        this.iban = iban;
        this.titulaire = titulaire;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getTitulaire() {
        return titulaire;
    }

    public void setTitulaire(String titulaire) {
        this.titulaire = titulaire;
    }

    @Override
    public String toString() {
        return "CompteBancaire [id=" + id + ", solde=" + solde + ", iban=" + iban + ", titulaire=" + titulaire + "]";
    }
    
}
