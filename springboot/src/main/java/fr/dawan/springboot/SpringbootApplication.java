package fr.dawan.springboot;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
public class SpringbootApplication {

	public static void main(String[] args) {
		// SpringApplication.run(SpringbootApplication.class, args);
	    SpringApplication app=new SpringApplication(SpringbootApplication.class);
	   // app.setAddCommandLineProperties(false);
	    app.run(args);
	}
	
    @Bean
    ModelMapper modelMapper(){
        return new ModelMapper();
    }
    
    @Bean
    PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
