package fr.dawan.springboot;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.entities.Produit;
import fr.dawan.springboot.enums.Conditionnment;
import fr.dawan.springboot.repositories.EmployeCustomRepository;
import fr.dawan.springboot.repositories.MarqueRepository;
import fr.dawan.springboot.repositories.ProduitRepository;

//@Component
//@Order(value = 1)
//@Transactional
public class RepositoryRunner implements ApplicationRunner {
    
    @Autowired
    private ProduitRepository prodRepository;

    @Autowired
    private MarqueRepository marqueRepository;
    
    @Autowired
    private EmployeCustomRepository employeRepository;
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("Repository Runner");
        List<Produit> lst=null;
        
//        lst=prodRepository.findAll();
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        System.out.println(prodRepository.count());

//        lst=prodRepository.findByPrixGreaterThan(30.0);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        lst=prodRepository.findByPrixBetween(40.0, 80.0);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        lst=prodRepository.findByPrixGreaterThanAndEmbalage(30.0, Conditionnment.PLASTIQUE);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        lst=prodRepository.findByDescriptionLikeIgnoreCase("M__%");
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        lst=prodRepository.findByPrixLessThanOrderByPrixDesc(40);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//        lst=prodRepository.findByPrixGrJPQL(90.0);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
        
//       lst=prodRepository.findByPrixGrSQL(200.0);
//        for(Produit pr : lst){
//            System.out.println(pr);
//        }
//        
//        lst=prodRepository.findByPrixGreaterThan(2.5, PageRequest.of(1, 4));
//            System.out.println(pr);
//        }
        
        
//      lst=prodRepository.findByPrixGreaterThan(2.5, PageRequest.of(1, 4,Sort.by("description").and(Sort.by("prix").descending())));
//      for(Produit pr : lst){
//          System.out.println(pr);
//      }
        
        
//        Page<Produit> page=prodRepository.findByPrixLessThan(3000.0, PageRequest.of(1, 3));
//        System.out.println(page.getTotalElements() + " " + page.isFirst() + " " + page.getTotalPages());
//        for(Produit pr : page.getContent()) {
//            System.out.println(pr);
//        }
//      lst=prodRepository.findByPrixGreaterThan(2.5,Sort.by("description").and(Sort.by("prix").descending()));
//      for(Produit pr : lst){
//          System.out.println(pr);
//      } 
      
//      lst=prodRepository.findTop5ByOrderByPrix();
//      for(Produit pr : lst){
//          System.out.println(pr);
//      } 
        
//        System.out.println(prodRepository.existsByPrix(40.0));
//        System.out.println(prodRepository.existsByPrix(42.0));
//        
//        System.out.println(prodRepository.countByEmbalage(Conditionnment.PLASTIQUE));
//        
//        marqueRepository.deleteByNom("Marque A");
//        
//        prodRepository.findAll().forEach(System.out::println);
        
        // Procedure Stocke
        //SQL
//        System.out.println(prodRepository.countByPrixSQL(50.0));
//        //Appel Explicite
//        System.out.println(prodRepository.countByPrixExp(50.0));
//        // Appel Implicite
//        System.out.println(prodRepository.get_count_by_prix2(50.0));
        
       // Repository personalisé
        employeRepository.findBy("John", null).forEach(System.out::println);
        employeRepository.findBy(null, "Doe").forEach(System.out::println);
        employeRepository.findBy("John", "Doe").forEach(System.out::println);
    }

}
