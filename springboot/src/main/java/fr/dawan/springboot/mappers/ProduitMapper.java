package fr.dawan.springboot.mappers;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

import fr.dawan.springboot.dto.ProduitDto;
import fr.dawan.springboot.entities.Produit;

@Component
public class ProduitMapper {

    private ModelMapper mapper;

    public ProduitMapper() {
        this.mapper = new ModelMapper();
    }

    public ProduitDto toDto(Produit pr) {
        mapper.typeMap(Produit.class, ProduitDto.class).addMappings(mapper -> {
            mapper.map(src -> src.getId(), ProduitDto::setId);
            mapper.map(src -> src.getDescription(), ProduitDto::setDescription);
            mapper.map(src -> src.getDateConception(), ProduitDto::setDateConception);
            mapper.map(src -> src.getPrix(), ProduitDto::setPrix);
            mapper.map(src -> src.getEmbalage(), ProduitDto::setEmbalage);
            mapper.map(src -> src.getImage(), ProduitDto::setImage);
            mapper.map(src -> src.getMarque(), ProduitDto::setMarque);
        });
        return mapper.map(pr, ProduitDto.class);
    }

    public Produit fromDto(ProduitDto prDto) {
        mapper.typeMap(ProduitDto.class, Produit.class).addMappings(mapper -> {
            mapper.map(src -> src.getId(), Produit::setId);
            mapper.map(src -> src.getDescription(), Produit::setDescription);
            mapper.map(src -> src.getDateConception(), Produit::setDateConception);
            mapper.map(src -> src.getPrix(), Produit::setPrix);
            mapper.map(src -> src.getEmbalage(), Produit::setEmbalage);
            mapper.map(src -> src.getImage(), Produit::setImage);
            mapper.map(src -> src.getMarque(), Produit::setMarque);
        });
        return mapper.map(prDto, Produit.class);
    }

}
