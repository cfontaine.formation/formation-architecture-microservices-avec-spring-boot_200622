package fr.dawan.springboot.circulardep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

//@Component
public class CircularRunner implements ApplicationRunner {

    @Autowired
    private ServiceA servA;

    @Autowired
    private ServiceB servB;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(servA);
        System.out.println(servB);
    }

}
