package fr.dawan.springboot.circulardep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//@Service
public class ServiceB {

    private ServiceA a;

    public ServiceB() {
       
    }
    
    @Autowired
    public ServiceB(ServiceA a) {
        this.a = a;
    }

    public ServiceA getA() {
        return a;
    }

    public void setA(ServiceA a) {
        this.a = a;
    } 
    
}
