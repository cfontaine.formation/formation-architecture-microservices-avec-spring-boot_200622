package fr.dawan.springboot.controllers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import fr.dawan.springboot.utils.ApiError;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<?> handleConflict(Exception ex,WebRequest request ) {
        ApiError err=new ApiError(HttpStatus.CONFLICT,ex.getMessage());
        return handleExceptionInternal(ex,err,new HttpHeaders(),HttpStatus.CONFLICT,request);
    }
}
