package fr.dawan.springboot.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ExempleMvcController {

    @GetMapping("/mvc/{msg}")
    public String getNom(Model model,@PathVariable String msg) {
        model.addAttribute("msg",msg);
        return "exemplevue";
    }
}
