package fr.dawan.formation;

@FunctionalInterface
public interface ICompare<T> {
   
   boolean comp(T a,T b);
   
}
