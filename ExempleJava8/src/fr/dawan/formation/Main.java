package fr.dawan.formation;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        int[] tab = { 3, 6, 2, -9, 4, 10 };
        Superieur sup = new Superieur();
        System.out.println(getValCmp(tab, sup));

        // 2. classe annonyme
        System.out.println(getValCmp(tab, new ICompare<Integer>() {

            @Override
            public boolean comp(Integer a, Integer b) {
                return a > b;
            }

        }));

        // 3. Expression Lambda
        System.out.println(getValCmp(tab, (v1, v2) -> v1 > v2));
        System.out.println(getValCmp(tab, (v1, v2) -> v1 < v2));
        
        // Interfaces fonctionnelles
        List<Integer> lst=Arrays.asList(4,7,2,9,3-6,-1);
        for(int i : lst) {
            System.out.println(i);
        }
        
        lst.forEach(i -> System.out.println(i));
        
        // Références de méthodes
        lst.forEach(System.out::println);
        
        // Stream
        List<Integer> resLst=lst.stream().map(i -> i*2).filter(v-> v>5).sorted().limit(3).collect(Collectors. toList());
        resLst.forEach(System.out::println);
        lst.stream().map(i -> i*2).filter(v-> v>5).sorted().limit(3).forEach(System.out::println);
        int res=lst.stream().map(i -> i*2).filter(v-> v>5).sorted().limit(3).reduce(0,(a,b)->a-b);
        System.out.println(res);
    }

    public static int getValCmp(int[] t, ICompare<Integer> cmp) {
        int m = t[0];
        for (int v : t) {
            if (cmp.comp(v, m)) {
                m = v;
            }
        }
        return m;
    }
}
