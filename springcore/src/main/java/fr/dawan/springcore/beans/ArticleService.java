package fr.dawan.springcore.beans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service(value = "serviceA") 	// <- @Service Componsant
@Lazy 							// <- Lazy loading	
//@Scope("prototype")
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;
	
	// @Autowired -> injection de dépendence automatique par type
	//				 required -> permet de préciser si l'injection d'une instance dans la propriété est obligatoire, par défaut à true
	//				 Autowired peut être placer sur un attribut, un constructeur ou un setter
     @Autowired /* (required = false) */
    // @Qualifier(value = "daoB")  // L'annotation @Qualifier permet de choisir le bean à injecter si il existe plusieurs du même type  
    private ArticleDao dao;

    public ArticleService() {
        System.out.println("ArticleService: Constructeur par défaut");
    }

    // @Autowired
    public ArticleService(/* @Qualifier(value = "daoB") */ ArticleDao dao) {
        this.dao = dao;
        System.out.println("ArticleService: Constructeur un paramètre");
    }

    public ArticleDao getDao() {
        return dao;
    }

    // @Autowired
    public void setDao(/* @Qualifier(value = "daoB") */ ArticleDao dao) {
        System.out.println("ArticleService: Setter ArticleDao ");
        this.dao = dao;
    }

    @PostConstruct
    public void init() {
        System.out.println("Méthode Init");
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Méthode destroy");
    }

    @Override
    public String toString() {
        return "ArticleService [dao=" + dao + "]";
    }

}
