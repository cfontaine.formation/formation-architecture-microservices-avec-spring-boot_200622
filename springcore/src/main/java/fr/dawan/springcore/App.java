package fr.dawan.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.springcore.beans.ArticleDao;
import fr.dawan.springcore.beans.ArticleService;

public class App {
    public static void main(String[] args) {
        // Création du conteneur
        ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);
        
        System.out.println("______________________________________________");
        
        // getBean permet de récupérer les instances des beans depuis le conteneur
        ArticleDao daoA = ctx.getBean("daoA", ArticleDao.class);
        System.out.println(daoA);
        // daoA -> singleton daoA et daoA2 même objet
        ArticleDao daoA2 = ctx.getBean("daoA", ArticleDao.class);
        System.out.println(daoA2);

        ArticleDao daoB = ctx.getBean("daoB", ArticleDao.class);
        System.out.println(daoB);
        // daoB => prototype daoB et daoB2 sont des objets différents
        ArticleDao daoB2 = ctx.getBean("daoB", ArticleDao.class);
        System.out.println(daoB2);

        ArticleService s1 = ctx.getBean("serviceA", ArticleService.class);
        System.out.println(s1);

        ArticleService s2=ctx.getBean("serviceB",ArticleService.class);
        System.out.println(s2);
        
        ArticleService s3=ctx.getBean("serviceC",ArticleService.class);
        System.out.println(s3);
        
        // Fermer le conteneur
        ((AbstractApplicationContext) ctx).close();
    }
}
