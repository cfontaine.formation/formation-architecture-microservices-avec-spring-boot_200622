package fr.dawan.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;

import fr.dawan.springcore.beans.ArticleDao;
import fr.dawan.springcore.beans.ArticleService;

@Configuration // <- Classe de configuration (fournit des définitions de bean)
@ComponentScan(basePackages = "fr.dawan.springcore") // <- indique les packages où seront recherché les composants 
public class AppConf {

    @Bean       // <- Déclaration d'un bean
    @Primary    // <- S'il y a plusieurs beans de type ArticleDao, c'est celui annoté avec @Primary qui sera choisi
                //    si il n'a pas de @Primary (ou de @Qualifier), une exception sera lancée
    @Lazy       // <- Lazy loading: bean singleton instancié à la première utilisation (uniquement pour ce bean).  
    public ArticleDao daoA() {  // <- bean qui a pour nom dans le conteneur daoA (nom de la méthode)
                                //    et de type ArticleDao (type de retour)
        return new ArticleDao("data1");
    }

    @Bean(name = "daoB")    // <- l'attribut name permet de modifier le nom du bean
    @Scope("prototype")     // <- prototype: crée une instance à chaque demande
    public ArticleDao dao() {
        return new ArticleDao("data2");
    }

    @Bean
    public ArticleService serviceB(ArticleDao daoA) {
        return new ArticleService(daoA);
    }
    
    @Bean
    public ArticleService serviceC() {
        return new ArticleService(dao());
    }
}
